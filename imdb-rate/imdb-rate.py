import sys
import threading
import logging
import argparse
import queue
import re
import time
import html
import requests
import queue
import json
from flask import Flask, request
from pymongo import MongoClient

sys.path.append("./common/python")
from service import Service
import config

service = Service('service.info.imdb')

mongo = MongoClient(config.config['mongo']['addr'])
db = mongo[service.name.replace('.', '_')]

tasks_queue = queue.Queue()

def get_imdb_data(name, year = None):
    url = 'http://www.omdbapi.com/?i=tt3896198&apikey=%s&plot=short&r=json&t=%s' % (config.config['omdbapi']['apikey'], html.escape(name))
    if year:
        url += '&y=%s' % year

    logging.info('Making request to %s', url)

    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as err:
        logging.error('IMDB request error %s', err)
        return None

    if response.status_code != 200:
        logging.debug("IMDB api call failed %d", response.status_code)
        return None

    result = response.json()

    if year and result['Response'] == "False":
        # try w/o year if the movie was not found with the year
        result = get_imdb_data(name)

    return result



#### ROUTERS ####

@service.api.route('/imdb/info', methods=['GET'])
def get_imdb_info():
    name = request.args.get('name')
    year = request.args.get('year')
    sender = request.args.get('_sender')

    if not name:
        return 'Name isnot specified', 400

    logging.info("Getting info for item '%s'", name)

    search_quary = {'name': name}
    if year:
        search_quary['year'] = year

    item = db.cache.find_one(search_quary)
    if not item and year:
        del search_quary['year']
        item = db.cache.find_one(search_quary)

    if item:
        logging.info("Return info from cache")
        return service.to_json(item['imdb']), 200

    get_imdb_info.counter += 1
    did = '%s-%d' % (service.name, get_imdb_info.counter)

    tasks_queue.put({
        'name': name,
        'year': year,
        'did': did,
        'sender': sender
    })

    logging.info("Return delayed call with id: %s", did)

    return service.to_json({'id':did}), 201

get_imdb_info.counter = 0
#### MESSAGES FILTER ####

#### MAIN ####

def tasks_queue_dispatcher(task):
    logging.info("Getting information for %s", task['name'])

    imdb = get_imdb_data(task['name'], task['year'])
    if not imdb or imdb['Response'] == "False":
        imdb = {}

    data = {
        'name': task['name'],
        'imdb': imdb,
        'created':time.time()
    }
    if task['year']:
        data['year'] = task['year']
    db.cache.insert_one(data)

    logging.info("Information for %s received, calling delayed call to '%s' \n%s",
                 task['name'], task['sender'], json.dumps(imdb, indent=4))

    service.call_service_post(
        task['sender'],
        '/deferred/%s' % task['did'],
        imdb
    )


if __name__ == '__main__':
    logging.info("Starting service...")

    service.add_queue_dispatcher('task_dispatcher', tasks_queue_dispatcher, tasks_queue)
    service.start()
