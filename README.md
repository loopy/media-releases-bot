# README #

The bot gets an information about new publications on NNMClub and posts them to telegram.

There are filters that allow to choose only interesting films. 

Just start the bot and explore it. Any changes are welcome!


## How to run ##

1. rename `config.json.in` to `config.json` and fill it
2. install all dependencies
3. run `runall.sh`