# README #

Service controlls all other services

### Service Name ###

service.controller

## API ##

### Services ###

#### GET /services ####
Returns a dictionary of current folders. 
List contains items:
```
{
    "[service name]": {
        "ip": [service addr],
        "port": [service port],
        "ttl": [ttl how often update info about service]
    }
    ...
}
```

#### GET /services/<service> ####
Returns an information about specific service.

#### POST /services/<service> ####
Updates information about the service. This call should be called every 30 seconds. If it is not called by service the controller will remove the service from a list of known services.
Parameters:
```
{
    "port": [service listening port],
    "listen": ["service name", "service name"]
    "ttl": [service ttl, not required]
}
```

#### DELETE /services/<service> ####
Removes a service from the list of known services.


### Events ###

#### POST /event/<event> ####
Fires an event. The data will b sent to all services that listen the event.
To listen it just listen POST /event/<event> on the service.


### Service Events ###

* event.service.connected
    * service: string

* event.service.disconnected
    * service: string
