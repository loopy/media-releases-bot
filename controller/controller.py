#! /usr/bin/env python3
import json
import logging
import datetime
import threading
import time
import argparse
import requests
from flask import Flask, request


SERVICE_NAME = 'service.controller'

DEFAULT_TTL = 30
DEFAULT_TIMEOUT = datetime.timedelta(seconds = 60)

logging.basicConfig(filename='logs/%s.log' % SERVICE_NAME,
                    format='%(asctime)s:' + SERVICE_NAME + ' %(levelname)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler())
app = Flask("controller")

is_shutdown = False

#### ROUTERS ####

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/shutdown', methods=['POST'])
def shutdown():
    logging.info("Will shutdown server")
    is_shutdown = True
    shutdown_server()
    return 'Server shutting down...'

#services

services = {}
services_ping = {}

events_hash = {}


#helpers
def send_event(event, data):
    if event not in events_hash:
        return
    for service in events_hash[event]:
        service_info = services[service]
        resend_to = 'http://%s:%d/event/%s' % (service_info['ip'], service_info['port'], event)
        logging.info("Send event '%s' to %s(%s:%d): %s",
                     event, service, service_info['ip'], service_info['port'], resend_to)
        requests.post(resend_to,
                      data=data,
                      headers={'Content-type': 'application/json', 'Accept': 'text/plain'})


def remove_service(service):
    del services[service]
    del services_ping[service]
    for event, receivers in events_hash.items():
        if service in receivers:
            receivers.remove(service)
    send_event('event.service.disconnected', '{"service":"%s"}' % service)


@app.route('/services', methods=['GET'])
def get_services():
    return json.dumps(services)

@app.route('/services/<service>', methods=['GET'])
def get_service(service):
    if service in services:
        info = json.dumps(services[service])
        logging.debug("Return: %s", info)
        return info
    logging.warning("No service %s found", service)
    return "", 400

@app.route('/services/<service>', methods=['POST'])
def post_service(service):
    info = request.get_json()
    port = info['port']
    ttl = DEFAULT_TTL
    if ttl in info:
        ttl = info['ttl']
    logging.info("Ping from service '%s' on %s:%d", service, request.remote_addr, port)
    send_connected = (service not in services or
                      services[service]['ip'] != request.remote_addr or
                      services[service]['port'] != port)

    services[service] = {'ip': request.remote_addr,
                         'port': port,
                         'ttl': ttl}

    services_ping[service] = datetime.datetime.now()
    if 'listen' in info:
        for event in info['listen']:
            if event not in events_hash:
                events_hash[event] = []
            if service not in events_hash[event]:
                events_hash[event].append(service)

    if send_connected:
        send_event('event.service.connected', '{"service":"%s"}' % service)

    return 'Ok'

@app.route('/services/<service>', methods=['DELETE'])
def delete_register(service):
    logging.info("Unregister service '%s'", service)
    remove_service(service)
    return 'Ok'

## events

@app.route('/event/<event>', methods=['POST'])
def post_event(event):
    logging.info("Received event '%s'", event)
    send_event(event, request.data)
    return 'Ok'


## check alive

def check_alive():
    while True:
        if is_shutdown:
            return

        to_remove = []
        now = datetime.datetime.now()
        for service, last_seen in services_ping.items():
            if last_seen + DEFAULT_TIMEOUT < now:
                to_remove.append(service)

        for service in to_remove:
            logging.info("Removing service '%s', because it does not send ping for a long time", service)
            remove_service(service)

        time.sleep(1)

#### MAIN ####

if __name__ == '__main__':
    logging.info("Starting service...")

    parser = argparse.ArgumentParser(description='Home-bot controller')
    parser.add_argument('-p', '--port', type=int, required=True, default=3030,
                        help='Port to run controller on')
    args = parser.parse_args()

    thread = threading.Thread(target=check_alive)
    thread.start()
    app.run(port=args.port, debug=False)
