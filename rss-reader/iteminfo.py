import re 

VIDEO_KEYS = [
    r'\d\d\d\d?(p|р)',
    r'DVDScr',
    r'HDRip',
    r'WEB-DLRip',
    r'CamRip',
    r'DVD-\d',
    r'SATRip',
    r'DVDScr',
    r'TVRip',
    r'BDRemux',
    r'BDRip',
    r'HDTV(Rip)?',
    r'DVDRip',
    r'SATRip',
    r'H.\d\d\d',
    r'WEBRip'
]

TV_SERIES_KEYS = [
    r'\Wсезон\W+(?P<season>\d+)\W+сери(и|я)\W+((?P<min_episode>\d+)-)?(?P<max_episode>\d+)\W.*из\s+(?P<total_episodes>[\d\?]+)\W*',
    r'\W(?P<season>\d+)\W+сезон\W+((?P<min_episode>\d+)-)?(?P<max_episode>\d+)\s+серии\s+из\D+(?P<total_episodes>[\d\?]+)\W*',
    r'\Wсезон\s+(?P<season>\d+)\W+выпуск\s+(?P<episode>\d+)\W*',
    r'\W+(выпуски|серии)\W+(?P<min_episode>\d+)-(?P<max_episode>\d+)\s+из\s+(?P<total_episodes>[\d\?]+)\W*',
    r'\W+(выпуски|серии)\W+(?P<min_episode>\d+)-(?P<max_episode>\d+)\W+',
    r'\W+(?P<max_episode>\d+)\s+из\s+(?P<total_episodes>[\d\?]+)\W*',
    r'\Wсезон\W+(?P<season>\d+)\W+серия\W+(?P<episode>\d+)',
    r'\Wсезон\W+(?P<season>\d+)\W+',
    r'\W(выпуск|серия)\W+(?P<episode>\d+)\W+'
]

MUSIC_KEYS = [
    r'FLAC',
    r'MP3',
    r'Lossless'
]

BOOK_KEYS = [
    r'DJVU',
    r'PDF',
    r'FB2'
]

IMAGE_KEYS = [
    r'JPG',
]

REG_EXPS = {
    'split_payload': re.compile(r'(^[^\[\(]+\S)\s+([\[\(].*)', re.I),
    'split_names': re.compile(r'\s*[/\|]\s*', re.I),
    'is_russian': re.compile(r'[йцукенгшщзхъфывапролджжэёячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ]'),
    'extract_year': re.compile(r'(20\d\d|19\d\d)')
}

video_keys_compiled = []
for k in VIDEO_KEYS:
    kc = re.compile(r'\W(' + k + r')(\W|$)', re.I)
    video_keys_compiled.append(kc)

tv_series_keys_compiled = []
for k in TV_SERIES_KEYS:
    kc = re.compile(k, re.I)
    tv_series_keys_compiled.append(kc)

def is_video(title):
    for kc in video_keys_compiled:
        if kc.search(title):
            return True
    return False

def safety_set_value(dict, dict_key, re_dict, group_name):
    try:
        if not re_dict.group(group_name):
            return

        if re_dict.group(group_name).find('?') >= 0:
            return

        dict[dict_key] = int(re_dict.group(group_name))

    except IndexError:
        pass


def check_tv_series(description, info):
    for key in tv_series_keys_compiled:
        re_series = key.search(description)
        if re_series:
            info['type'] = 'tv-series'
            info['episodes'] = {}
            safety_set_value(info['episodes'], 'season', re_series, 'season')
            safety_set_value(info['episodes'], 'episode', re_series, 'episode')
            safety_set_value(info['episodes'], 'episode_from', re_series, 'min_episode')
            safety_set_value(info['episodes'], 'episode_to', re_series, 'max_episode')
            safety_set_value(info['episodes'], 'total', re_series, 'total_episodes')

            if 'episode_from' not in info['episodes'] and 'episode_to' in info['episodes']:
                info['episodes']['episode_from'] = 1

            return True
    return False

def get_tv_series_info(payload):
    items = re.search(r'\W(\d+)\D+(\d+)-(\d+)\D+(\d+)\W', payload)


def get_video_info(title):
    result = {
        'type': 'movie',
        'name': {}
    }

    objects = REG_EXPS['split_payload'].search(title)
    if objects is None:
        return

    payload = objects.group(1)
    description = objects.group(2)

    names = REG_EXPS['split_names'].split(payload)

    # eng is the shortest line
    short_name = ""
    for name in names:
        if REG_EXPS['is_russian'].search(name):
            result['name']['ru'] = name
        elif short_name == "" or len(short_name) > len(name):
            short_name = name

    if len(short_name) > 0:
        result['name']['en'] = short_name

    if len(names) > 2:
        if 'ru' in result['name']:
            names.remove(result['name']['ru'])
        if 'en' in result['name']:
            names.remove(result['name']['en'])
        result['name']['original'] = names[0]

    year_re = REG_EXPS['extract_year'].search(description)
    if year_re:
        result['year'] = int(year_re.group(1))

    for key in VIDEO_KEYS:
        quality_re = re.search(r'(' + key + r'[^,\]\)\s]*)([,\]\)\s]|$)', description)
        if quality_re:
            result['quality'] = quality_re.group(1)
            break

    check_tv_series(description, result)

#    if check_tv_series(description, result):
#        print(result)

    return result


def parse_item(title, description):
    if is_video(title):
        return get_video_info(title)


if __name__ == '__main__':
    with open('sample.txt') as fp:
        for line in fp:
            title = line.rstrip()
            if is_video(title):
                get_video_info(title)