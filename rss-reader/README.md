# README #

Service to read rss from nnmclub

### Service Name ###
    
service.rss-reader.nnmclub

### Service Events ###

* event.torrent.item
    * title: string
    * link: string
    * date_created: int (unix timestamp)
    * description: string
