#! /usr/bin/env python3

import threading
import time
import sys
import datetime
import logging
import html
import re
import feedparser
from pymongo import MongoClient

from iteminfo import parse_item

sys.path.append("./common/python")
from service import Service, ServiceListener
import config

import ssl

if hasattr(ssl, '_create_unverified_context'):
    ssl._create_default_https_context = ssl._create_unverified_context

mongo = MongoClient(config.config['mongo']['addr'])
item_db = mongo['items']

delayed_requests_counters = {}

RSS_URL = 'https://nnmclub.to/forum/rssp.xml'

service = Service('service.rss-reader.nnmclub')

if 'last_item_date' not in service.storage.data:
    service.storage.data['last_item_date'] = 0
logging.info("Last item date is %d", service.storage.data['last_item_date'])


def check_item_delayed_requests_number(item):
    if item['_drn'] == 0:
        del item['_drn']
        notify_about_new_item(item)

class RssReaderServiceListener(ServiceListener):
    def on_response(self, service, api, response, err, item):
        item['_drn'] -= 1
        if not err:
            if service == 'service.info.nnm-club':
                item['files'] = response
            elif service == 'service.info.imdb':
                if 'imdbID' in response:
                    item['imdb'] = response
        else:
            logging.error('Delayed call responce from %s with error %s', service, err)

        check_item_delayed_requests_number(item)


def notify_about_new_item(item):
    obj = item_db.items.insert_one(item)
    item['_id'] = obj.inserted_id
    service.send_event("event.torrent.item", item)


def extract_nnm_id(url):
    # https://nnm-club.ws/download.php?id=898662
    result = re.search(r'\?id=(\d+)$', url)
    if not result:
        return None
    return result.group(1)


def get_item_info_and_notify(item):
    image_obj = re.search(r'<img.*src="([^"]+)"', item['rss']['description'], re.M|re.I)
    if image_obj:
        item['info']['image'] = image_obj.group(1)
        logging.debug('Item image url: %s', item['info']['image'])

    parce_info = parse_item(item['rss']['title'], item['rss']['description'])
    if parce_info:
        logging.info('Parsed more info from the title, adding it')
        item['info'].update(parce_info)

    item['_drn'] = 1 # it is like an reference counting

    if ('type' in item['info'] and
        item['info']['type'] in ['tv-series', 'movie']):

        r_info = {}
        if 'en' in item['info']['name']:
            r_info['name'] = item['info']['name']['en']
        else:
            notify_about_new_item(item)
            return
        
        if 'year' in item['info']:
            r_info['year'] = item['info']['year']

        item['_drn'] += 1
        service.call_service_get('service.info.imdb',
                                    '/imdb/info',
                                    r_info,
                                    item
                                    )

    if item['rss']['torrent']:
        item['mmn_id'] = extract_nnm_id(item['rss']['torrent'])
        logging.info('Getting torrent size info for torrent url %s, nnm_id = %s', item['rss']['torrent'], item['mmn_id'])
        item['_drn'] += 1
        service.call_service_get('service.info.nnm-club',
                                    '/size',
                                    {'nnm_id': item['mmn_id']},
                                    item
                                    )
    item['_drn'] -= 1
    check_item_delayed_requests_number(item)


def rss_reader():
    logging.info("Downloading rss from %s", RSS_URL)
    ttl = 1

    feed = feedparser.parse(RSS_URL)
    if feed.bozo == 0:
        logging.info("Downloaded total items: %d, ttl: %s mins",
                     len(feed['items']),
                     feed['channel']['ttl'])
        max_item_date = 0
        for item in feed['items']:
            date = time.mktime(item['published_parsed'])
            title = item['title']
            link = item['link']
            torrent_url = None

            if 'links' in item:
                torrent_link = [i for i in item['links'] if i['rel'] == 'enclosure']
                if len(torrent_link) > 0:
                    torrent_url = torrent_link[0]['href']

            if service.storage.data['last_item_date'] >= date:
                continue

            # ignore advertising
            if datetime.datetime.fromtimestamp(date) > datetime.datetime.now():
                continue

            logging.info("%d: %s\tLink: %s\tTorrent: %s", date, title, link, torrent_url)
            max_item_date = max(max_item_date, date)
            description = html.unescape(item['description'])
            item = {'rss': {
                'title': title,
                'link': link,
                'date_created': date,
                'description': description,
                'torrent': torrent_url
                },
                    'info':{}}

            get_item_info_and_notify(item)

        ttl = int(feed['channel']['ttl']) * 2 // 3 * 60
        service.storage.data['last_item_date'] = max(max_item_date,
                                                     service.storage.data['last_item_date'])
        service.storage.sync()
    else:
        logging.error("Error downloading rss %s: %s", RSS_URL, feed.bozo_exception)

    logging.info("Will check rss in %d min, last item date = %d",
                 ttl/60, service.storage.data['last_item_date'])
    return datetime.timedelta(seconds=ttl)

#### MAIN ####

if __name__ == '__main__':
    service.add_periodic_call('rss_reader', rss_reader)
    service.start(listener=RssReaderServiceListener())
