mkdir data
mkdir logs

python3 controller/controller.py --port 3030 > /dev/null 2>&1 &
python3 telegram/telegram-bot.py > /dev/null 2>&1 &
python3 chat-notifier/chat-notifier.py > /dev/null 2>&1 &
python3 torrent-filter/torrent-filter.py > /dev/null 2>&1 &
python3 imdb-rate/imdb-rate.py > /dev/null 2>&1 &
python3 nnm-club/nnm-club.py > /dev/null 2>&1 &
python3 rss-reader/rss-reader.py > /dev/null 2>&1 &
