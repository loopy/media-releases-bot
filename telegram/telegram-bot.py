import sys
import logging
import time
from pymongo import MongoClient
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
import telegram
from flask import request

sys.path.append("./common/python")
from service import Service, ServiceListener
import config

service = Service('service.telegram.bot', ["event.service.disconnected"])

mongo = MongoClient(config.config['mongo']['addr'])
db = mongo[service.name.replace('.', '_')]

#### TELEGRAM ####
telegramBot = telegram.Bot(token=config.config["telegram-bot"]["token"])
telegramUpdater = Updater(token=config.config["telegram-bot"]["token"])
telegramDispatcher = telegramUpdater.dispatcher

def is_registered(chat_id):
    chart = db.chats.find_one({'chat_id': chat_id})
    return chart is not None and chart['authorized']

def on_command_start(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Tell me the secret word!")

def on_command_help(bot, update):
    if is_registered(update.message.chat_id):
        message = "You are registered user\nAvaliable commands:\n"
        message += "<b>/help</b> - show this help\n"
        message += "<b>/start</b> - connect to home server\n"
        for command, info in commands.items():
            message += "<b>/%s</b> - %s\n" %(command, info['description'])
    else:
        message = "You are not registered user, please type /start to register"
    bot.sendMessage(chat_id=update.message.chat_id, text=message, parse_mode='HTML')

def on_text(bot, update):
    if not is_registered(update.message.chat_id):
        if config.config["telegram-bot"]["secret-word"] == update.message.text:
            db.chats.insert_one({
                'chat_id': update.message.chat_id,
                'authorized': True,
                'created': time.time()
            })
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="Yeah! You will receive notifications")
        else:
            bot.sendMessage(chat_id=update.message.chat_id,
                            text="Not correct, please try again")
        return
    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text="You are already registered. Type /help to see a list of available commands")
    #here is loginned user

commands = {}

def on_registered_command_received(bot, update):
    logging.info("User entered command %s", update.message.text)
    for command, info in commands.items():
        if update.message.text.startswith('/' + command):
            message = ''
            if len(update.message.text) > len(command) + 2:
                message = update.message.text[len(command) + 1:].strip()
            service.call_service_post(info['service'],
                                      '/telegram/%s' % command, {
                                          'message': message,
                                          'chat_id': update.message.chat_id
                                      })


def setup_telegram():
    telegramDispatcher.add_handler(CommandHandler('start', on_command_start))
    telegramDispatcher.add_handler(CommandHandler('help', on_command_help))
    telegramDispatcher.add_handler(MessageHandler(Filters.text, on_text))
    telegramUpdater.start_polling()

def get_chat_ids(id):
    chats = []
    if id == "all":
        cursor = db.chats.find({}, {'chat_id':1, '_id':0})
        for item in cursor:
            chats.append(item['chat_id'])
    else:
        chats = [int(id)]
    return chats

#### ROUTERS ####

@service.api.route('/chat/<id>/message', methods=['POST'])
def post_chat_message(id):
    logging.info("Posting message to chat_id '%s'", id)
    info = request.get_json()

    for chat_id in get_chat_ids(id):
        try:
            message = telegramBot.sendMessage(chat_id=chat_id,
                                              text=info["message"],
                                              parse_mode='HTML')
            logging.info('Message %d is sent to chat %d', message['message_id'], chat_id)
        except telegram.error.BadRequest as error:
            logging.error('Message was not send to chat %d:\n\tmessage: %s\n\texception: %s',
                          chat_id, info["message"], error)
    return 'Ok'

@service.api.route('/chat/<id>/photo', methods=['POST'])
def post_chat_photo(id):
    logging.info("Posting photo to chat_id '%s'", id)
    info = request.get_json()

    for chat_id in get_chat_ids(id):
        try:
            message = telegramBot.sendPhoto(chat_id=chat_id,
                                            photo=info["url"])
            logging.info('Photo %d is sent to chat %d', message['message_id'], chat_id)
        except telegram.error.BadRequest as error:
            logging.error('Photo was not send to chat %d:\n\tmessage: %s\n\texception: %s',
                          chat_id, info["message"], error)
    return 'Ok'


@service.api.route('/command/<command>', methods=['POST'])
def post_chat_command(command):
    logging.info("Register new command '%s'", command)
    info = request.get_json()
    commands[command] = {'service': info['service'],
                         'description': info['description']}
    telegramDispatcher.add_handler(CommandHandler(command, on_registered_command_received))
    return 'Ok'


#### ServiceListener class ####
class TelegramServiceListener(ServiceListener):
    def on_shutdown(self):
        logging.info('Stopping telegram updater')
        telegramUpdater.stop()

    def on_event(self, name, data):
        if name == "event.service.disconnected":
            for k, v in list(commands.items()):
                if v['service'] == data['service']:
                    del commands[k]
        else:
            logging.error('Received unknown command %s', name)

#### MAIN ####

if __name__ == '__main__':
    logging.info("Starting service...")
    setup_telegram()
    service.start(listener=TelegramServiceListener())
