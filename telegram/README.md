# README #

Service to contact telegram bot

### Service Name ###
    
service.telegram.bot


## API ##

### chat ###

#### POST /chat/<id>/message ####
Posts message to the chat. `id` could be any registered chat id. 
Use key word "all" as `id` to post the message to all registered chats.
Parameters:
```
{
    "message": [string, message to post],
    "type": ["HTML", "Markdown", "None"]
}
```


#### POST /command/<command> ####
Registers a service as command dispatcher
Parameters:
```
{
    "service": [string, service that will dispatch a commans],
    "description": [string, command description]
}
```

When user enters the command Telegram Bot will do the following call:
```
POST /telegram/<command>
{
    'message': [string, message that user enters without the command]
    'chat_id': [int, id of the chat]
}
```