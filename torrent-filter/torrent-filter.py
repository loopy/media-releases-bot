import sys
import threading
import logging
import argparse
import queue
import re
import time
import html
from flask import Flask, request
from pymongo import MongoClient

sys.path.append("./common/python")
from service import Service, ServiceListener
import config

service = Service('service.torrent.filter', ['event.torrent.item', 'event.service.connected'])

mongo = MongoClient(config.config['mongo']['addr'])
db = mongo[service.name.replace('.', '_')]

#### ROUTERS ####

def try_add_rule(info):
    message = info['message']

    if len(message) == 0:
        return 'Rule could not be empty'

    arr = message.split('|')
    pattern = arr[0]
    name = arr[-1]

    logging.info("Trying to add rule '%s' with name '%s'", pattern, name)

    if len(pattern) == 0:
        return 'Rule could not be empty'

    try:
        re.compile(pattern)
    except re.error:
        return 'Rule is invalid regexp'

    if db.filters.find_one({
            'rule': pattern,
            'chat_id': info['chat_id']
        }):
        return 'Rule like this was already added'

    db.filters.insert_one({
        'name': name,
        'rule': pattern,
        'chat_id': info['chat_id'],
        'created':time.time()
    })
    return 'Rule "%s" added' % name


def try_remove_rule(info):
    name = info['message']

    if len(name) == 0:
        return 'Please specify a name'

    result = db.filters.delete_one({
        'name': name,
        'chat_id': info['chat_id']
    })

    if result.deleted_count != 1:
        return "Could not find the rule"

    return "Rule '%s' deleted" % name


def set_imdb_level(info):
    level = info['message']

    if len(level) == 0:
        return 'Please specify level'

    float_level = 0

    try:
        float_level = float(level)
    except ValueError:
        return "Incorrect value"

    db.rates.update_one({
            'chat_id': info['chat_id']
        }, { "$set": {
            'imdb': float_level,
            'chat_id': info['chat_id'],
            'created': time.time()
        }},
        upsert=True
    )

    if float_level == 0:
        return "IMDB filter is disabled"
    else:
        return "Items with IMDB rate more then %.2f will be shown" % float_level


@service.api.route('/telegram/<command>', methods=['POST'])
def post_telegram_rules(command):
    logging.info("Telegram command %s", command)
    info = request.get_json()
    message = "Unknown command"

    if command == 'rules':
        message = "Torrent filter rules:\n"
        cursor = db.filters.find({'chat_id': info['chat_id']})
        for torrent_filter in cursor:
            if torrent_filter['name'] == torrent_filter['rule']:
                message += "    <b>%s</b>\n" % (torrent_filter['name'])
            else:
                message += "    <b>%s</b>( %s )\n" % (
                    torrent_filter['name'], torrent_filter['rule'])

        found = False
        cursor = db.rates.find({'chat_id': info['chat_id']})
        for rate_filter in cursor:
            if rate_filter['imdb'] > 0:
                found = True
                message += "\nItems with IMDB rate more then %.2f will be shown\n" % rate_filter['imdb']
        if not found:
            message += "\nIMDB rate filter is not set\n"


    elif command == 'follow':
        message = try_add_rule(info)

    elif command == 'unfollow':
        message = try_remove_rule(info)

    elif command == 'imdb':
        message = set_imdb_level(info)

    else:
        logging.warning("Unknown command")
        return "Unknown command", 404

    service.call_service_post('service.telegram.bot',
                              '/chat/%d/message' % info['chat_id'],
                              {'message': message})
    return 'Ok'

#### MESSAGES FILTER ####

#### TORRENT FILTER CONTROLLER

class TorrentFilterServiceListener(ServiceListener):
    def _register_telegram_commands(self):
        service.call_service_post('service.telegram.bot',
                                  '/command/rules', {
                                      'service': service.name,
                                      'description': 'show all torrent rules'
                                  })
        service.call_service_post('service.telegram.bot',
                                  '/command/follow', {
                                      'service': service.name,
                                      'description': 'add new torrent rule, usage "/follow [name]"'
                                  })
        service.call_service_post('service.telegram.bot',
                                  '/command/unfollow', {
                                      'service': service.name,
                                      'description': 'remove torrent rule, usage "/unfollow [name]"'
                                  })
        service.call_service_post('service.telegram.bot',
                                  '/command/imdb', {
                                      'service': service.name,
                                      'description': 'Set IMDB rate lavel, usage "/imdb [level]", default 0'
                                  })

    def _chats_that_maches(self, data):
        logging.info("Checking item %s", data['rss']['title'])

        chats = []
        cursor = db.filters.find()
        for torrent_filter in cursor:
            if re.search(torrent_filter['rule'], data['rss']['title'], re.IGNORECASE):
                logging.info("  Matching rule %s for chat %s", torrent_filter['rule'], torrent_filter['chat_id'])
                chats.append(torrent_filter['chat_id'])

        if 'imdb' in data and 'imdbRating' in data['imdb']:
            try:
                item_imdb_rate = float(data['imdb']['imdbRating'])
                cursor = db.rates.find()
                for rate_filter in cursor:
                    logging.info("  Item rate = %.2f filter rate = %.2f", item_imdb_rate, rate_filter['imdb'])
                    if (rate_filter['imdb'] > 0 and
                            rate_filter['imdb'] <= item_imdb_rate and
                            rate_filter['chat_id'] not in chats):
                        logging.info("  IMDB matches for chat_id = %s", rate_filter['chat_id'])
                        chats.append(rate_filter['chat_id'])
            except ValueError:
                pass
        else:
            logging.info("  No imdb rating found")

        return chats

    def on_controller_connected(self):
        self._register_telegram_commands()

    def on_event(self, name, data):
        if name == 'event.service.connected':
            if data['service'] == 'service.telegram.bot':
                self._register_telegram_commands()
        elif name == 'event.torrent.item':
            for user_id in self._chats_that_maches(data):
                logging.info("New matching item found: %s", data['rss']['title'])
                service.send_event("event.item.match", {
                    'user_id': user_id,
                    'item_id': data['_id']
                })


#### MAIN ####

if __name__ == '__main__':
    logging.info("Starting service...")
    service.start(listener=TorrentFilterServiceListener())
