import sys
import threading
import logging
import argparse
import queue
import re
import time
import html
import requests
import queue
import json
import re
from flask import Flask, request
from pymongo import MongoClient

sys.path.append("./common/python")
from service import Service
import config

service = Service('service.info.nnm-club')

mongo = MongoClient(config.config['mongo']['addr'])
db = mongo[service.name.replace('.', '_').replace('-', '_')]

tasks_queue = queue.Queue()

def get_files_data(nnm_id):
    url = 'https://nnmclub.to/forum/filelst.php?attach_id=%s' % nnm_id

    logging.info('Making request to %s', url)

    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as err:
        logging.error('NNM getting size call error %s', err)
        return None

    if response.status_code != 200:
        logging.info("NNM getting size call failed %d", response.status_code)
        return None

    result = []
    html_text = response.text
    for match in re.finditer('<tr[^>]+><td[^>]+>([^<]*)</td><td[^>]+>([^<]*)</td><td[^>]+>([^<]*)</td></tr>', html_text, re.S):
        obj = match.group(1)
        name = match.group(2)
        size = match.group(3)
        if obj == " Folder":
            continue

        size = re.sub(r'[ ,\.]', '', size)
        if len(size) > 0:
            size = int(size)
        else:
            size = 0

        result.append({
            'name': name,
            'size': size
        })

    return result



#### ROUTERS ####

@service.api.route('/size', methods=['GET'])
def get_torrent_size():
    nnm_id = request.args.get('nnm_id')
    sender = request.args.get('_sender')

    if not nnm_id:
        return 'NoNaMe id is not specified', 400

    logging.info("Getting info for item '%s'", nnm_id)

    search_quary = {'nnm_id': nnm_id}

    item = db.cache.find_one(search_quary)

    if item:
        logging.info("Return info from the cache")
        return service.to_json(item['files']), 200

    get_torrent_size.counter += 1
    did = '%s-%d' % (service.name, get_torrent_size.counter)

    tasks_queue.put({
        'nnm_id': nnm_id,
        'did': did,
        'sender': sender
    })

    logging.info("Return delayed call with id: %s", did)

    return service.to_json({'id':did}), 201

get_torrent_size.counter = 0
#### MESSAGES FILTER ####

#### MAIN ####

def tasks_queue_dispatcher(task):
    logging.info("Getting information item %s", task['nnm_id'])

    files_info = get_files_data(task['nnm_id'])

    data = {
        'nnm_id': task['nnm_id'],
        'files': files_info,
        'created':time.time()
    }
    db.cache.insert_one(data)

    logging.info("Information for %s received, calling delayed call to '%s' \n%s",
                 task['nnm_id'], task['sender'], json.dumps(files_info, indent=4))

    service.call_service_post(
        task['sender'],
        '/deferred/%s' % task['did'],
        files_info
    )


if __name__ == '__main__':
    logging.info("Starting service...")

    service.add_queue_dispatcher('task_dispatcher', tasks_queue_dispatcher, tasks_queue)
    service.start()
