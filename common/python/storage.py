import json
import os
import os.path

class LocalStorage:
    def __init__(self, filename):
        self.filename = filename
        if os.path.isfile(filename):
            with open(filename, "r") as file:
                content = file.readline()
                if len(content) > 0:
                    self.data = json.loads(content)

        if not hasattr(self, 'data'):
            self.data = {}

    def sync(self):
        with open(self.filename, "w") as file:
            json.dump(self.data, file)
