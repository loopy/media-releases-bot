import logging
import requests
import datetime
import time
import threading
import socket
import argparse
import queue
import json
import urllib
from bson import json_util
from flask import Flask, request
from storage import LocalStorage
import config

class ServiceListener:
    def on_controller_connected(self):
        pass

    def on_controller_disconnected(self):
        pass

    def on_shutdown(self):
        pass

    def on_event(self, name, data):
        pass

    def on_response(self, service, api, response, err, context):
        pass

class Service:
    @staticmethod
    def find_empty_port():
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind(('localhost', 0))
        port = sock.getsockname()[1]
        sock.close()
        return port

    def __init__(self, name, listen_events=None):
        self.name = name
        self.listen_events = listen_events
        self._setup_logging()

        self.headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        self.connected = False
        self.cache = []
        self.services = {}
        self.controller = None
        self.events_thread = None
        self.port = None
        self.threads = {}
        self.exiting = False
        self.callbacks_queue = queue.Queue()
        self._local_storage = None
        self._init_api()
        self.deffered_calls = {}
        self._parse_arguments()
        self.listener = ServiceListener()
        if self.args.controller is not None:
            self.controller = self.args.controller
        elif 'controller' in config.config and 'addr' in config.config['controller']:
            self.controller = config.config['controller']['addr']
        else:
            self.controller = 'http://localhost:3030'
        logging.info('service: Will connect to controller %s', self.controller)
        self.port = self.args.port


    def start(self, listener=None):
        if listener:
            self.listener = listener

        if self.port is None:
            self.port = Service.find_empty_port()
        logging.info('service: Run on port %s', self.port)

        self.add_queue_dispatcher("callbacks_queue_dispatcher", self._callbacks_queue_dispatcher, self.callbacks_queue)
        self.add_periodic_call("ping_controller", self._ping_controller)

        self.api.run(port=self.port, debug=False)


    def add_periodic_call(self, name, func, delay=datetime.timedelta(seconds=1)):
        self.threads[name] = {
            'func': func,
            'delay': delay
        }
        self.threads[name]['thread'] = threading.Thread(target=self._thread_func, args=(name,))
        self.threads[name]['thread'].start()

    def add_queue_dispatcher(self, name, func, queue):
        self.threads[name] = {
            'func': func,
            'queue': queue
        }
        self.threads[name]['thread'] = threading.Thread(target=self._queue_func, args=(name,))
        self.threads[name]['thread'].start()

    def send_event(self, event_name, data, cache=False):
        logging.debug("service: Send event %s", event_name)
        if self.connected:
            requests.post("%s/event/%s" % (self.controller, event_name),
                          data=json.dumps(data, default=json_util.default),
                          headers=self.headers)
        elif cache:
            logging.debug("service: Cache event %s", event_name)
            self.cache.append({'event':event_name,
                               'data':data})
        else:
            logging.debug("service: Ignore event %s, because controller is not connected", event_name)


    def call_service_post(self, service, api, data):
        logging.debug("service: Call %s%s", service, api)
        if not self.connected:
            return None

        service_addr = self._get_service_addr(service)
        if service_addr is None:
            logging.debug("service: Unabel to call %s%s: requested service is not connected", service, api)
            return None

        url = "http://%s:%d%s" % (service_addr["ip"], service_addr["port"], api)
        logging.debug("service: do call %s", url)
        return requests.post(url,
                             data=json.dumps(data, default=json_util.default),
                             headers=self.headers)


    def call_service_get(self, service, api, parameters={}, context=None):
        logging.debug("service: Call GET %s%s", service, api)
        if not self.connected:
            self.listener.on_response(service, api, None, 'Not connected', context)
            return None

        service_addr = self._get_service_addr(service)
        if service_addr is None:
            logging.debug("service: Unabel to call %s%s: requested service is not connected", service, api)
            self.listener.on_response(service, api, None, 'Destination service is not running', context)
            return None

        parameters['_sender'] = self.name

        url = "http://%s:%d%s?%s" % (service_addr["ip"],
                                     service_addr["port"],
                                     api,
                                     urllib.parse.urlencode(parameters))
        logging.debug("service: do call %s", url)
        r = requests.get(url, headers=self.headers)

        if r.status_code == 200:
            self.listener.on_response(service, api, r.json(), None, context)
        elif r.status_code == 201:
            r_json = r.json()
            self.deffered_calls[r_json['id']] = {
                'service': service,
                'api': api,
                'context': context
            }
        else:
            self.listener.on_response(service, api, None, r.status_code, context)


    def to_json(self, data):
        return json.dumps(data, default=json_util.default)

    @property
    def storage(self):
        if self._local_storage is None:
            self._local_storage = LocalStorage('data/%s.dat' % self.name)
        return self._local_storage

#### PRIVATE METHODS ####

    def _init_api(self):
        self.api = Flask(self.name.replace('.', '_'))
        @self.api.route('/shutdown', methods=['POST'])
        def shutdown():
            logging.info("service: Will shutdown server")
            self._do_shutdown()
            return 'Server shutting down...'

        @self.api.route('/event/<event>', methods=['POST'])
        def post_event(event):
            logging.info("Received event '%s'", event)
            self.callbacks_queue.put({
                'type': 'event',
                'name': event,
                "data": json.loads(request.data.decode(), object_hook=json_util.object_hook)
            })
            return 'Ok'

        @self.api.route('/deferred/<did>', methods=['POST'])
        def deferred_call(did):
            logging.info("Received deffered call '%s'", did)
            self.callbacks_queue.put({
                'type': 'deffered_call',
                'did': did,
                "data": json.loads(request.data.decode(), object_hook=json_util.object_hook)
            })
            return 'Ok'


    def _setup_logging(self):
        logging.basicConfig(filename='logs/%s.log' % self.name,
                            format='%(asctime)s:' + self.name + ' %(levelname)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p',
                            level=logging.INFO)
        logging.getLogger().addHandler(logging.StreamHandler())


    def _parse_arguments(self):
        parser = argparse.ArgumentParser(description='Home-bot service %s' % self.name)
        parser.add_argument('-c', '--controller', type=str, required=False,
                            help='Controler address and port (default is http://localhost:3030)')
        parser.add_argument('-p', '--port', type=int, required=False,
                            help='Port to run on (default is random)')
        self.args = parser.parse_args()


    def _do_shutdown(self):
        logging.info('service: Service %s is sutting down', self.name)
        self.exiting = True
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()
        self.listener.on_shutdown()


    def _thread_func(self, name):
        logging.error('service: Starting thread %s', name)
        if name not in self.threads:
            logging.error('service: Thread name %s is not found', name)

        thread_info = self.threads[name]
        thread_info['last_call'] = None

        while not self.exiting:
            if (thread_info['last_call'] is None or
                    datetime.datetime.now() - thread_info['last_call'] > thread_info['delay']):
                new_delay = thread_info['func']()
                if new_delay:
                    thread_info['delay'] = new_delay
                thread_info['last_call'] = datetime.datetime.now()
            time.sleep(1)

        logging.error('service: Finish thread %s', name)


    def _queue_func(self, name):
        logging.info('service: Starting queue thread %s', name)
        if name not in self.threads:
            logging.error('service: Thread name %s is not found', name)

        queue_info = self.threads[name]

        while True:
            try:
                item = queue_info['queue'].get(block=True, timeout=1)
                try:
                    queue_info['func'](item)
                except Exception as e:
                    logging.error('service: exception occured queue "%s" message processing: %s', name, e)

                queue_info['queue'].task_done()
            except queue.Empty:
                if self.exiting:
                    break

        logging.info('service: Finish queue thread %s', name)


    def _callbacks_queue_dispatcher(self, callback):
        if callback['type'] == 'event':
            logging.info("Received event '%s'", callback['name'])
            self.listener.on_event(callback['name'], callback['data'])
        elif callback['type'] == 'deffered_call':
            logging.info("Received deffered call id '%s'", callback['did'])
            if callback['did'] in self.deffered_calls:
                info = self.deffered_calls[callback['did']]
                self.listener.on_response(info['service'], info['api'],
                                          callback['data'], None, info['context'])
                del self.deffered_calls[callback['did']]
            else:
                logging.error('Unknown deffered call id "%s"', callback['did'])
        else:
            logging.error('Received unknown callback type "%s"', callback['type'])


    def _do_controller_connected(self):
        logging.info("service: Controller %s is connected", self.name)
        for event in self.cache:
            self.send_event(event['event'], event['data'])
        self.listener.on_controller_connected()


    def _do_controller_disconnected(self):
        logging.info("service: Controller %s is disconnected", self.name)
        self.listener.on_controller_disconnected()


    def _ping_controller(self):
        try:
            json = {'port': self.port}
            if self.listen_events != None:
                json['listen'] = self.listen_events
            requests.post("%s/services/%s" % (self.controller, self.name),
                          json=json,
                          headers=self.headers)
            if not self.connected:
                self.connected = True
                self._do_controller_connected()
            return datetime.timedelta(seconds=30)

        except requests.exceptions.RequestException:
            if self.connected:
                self.connected = False
                self._do_controller_disconnected()
            return datetime.timedelta(seconds=1)


    def _get_service_addr(self, service):
        if service in self.services and self.services[service]['expired'] > datetime.datetime.now():
            return self.services[service]

        logging.debug("service: Updating address for service %s", service)

        responce = requests.get("%s/services/%s" % (self.controller, service))
        if responce.status_code != 200:
            logging.debug("service: Service %s not found, status code %d",
                          service,
                          responce.status_code)
            return None

        info = responce.json()
        info['expired'] = datetime.datetime.now() + datetime.timedelta(seconds=info['ttl'])
        self.services[service] = info

        logging.debug("service: Service %s has address %s:%d", service, info["ip"], info["port"])

        return info
