import sys
import threading
import logging
import argparse
import queue
import re
import time
import json
import html
from bson import json_util
from flask import Flask, request
from pymongo import MongoClient

sys.path.append("./common/python")
from service import Service, ServiceListener
import config

service = Service('service.chat.notifier', ['event.item.match'])

mongo = MongoClient(config.config['mongo']['addr'])
db = mongo[service.name.replace('.', '_')]
item_db = mongo['items']


def format_file_size(num, suffix='B'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Y', suffix)

#### TORRENT FILTER CONTROLLER

class TorrentFilterServiceListener(ServiceListener):
    def _post_telegram(self, item, user_id):

        logging.info("Notify item: %s", json.dumps(item, indent=4, default=json_util.default))

        if 'image' in item['info']:
            service.call_service_post('service.telegram.bot',
                                      '/chat/%s/photo' % user_id, {
                                          'url': item['info']['image']
                                      })
        message_html_text = "<a href=\"%s\">%s</a>" % (
                                item['rss']['link'],
                                html.escape(item['rss']['title'])
                            )

        if 'name' in item['info']:
            if 'en' in item['info']['name']:
                message_html_text += '\nEnglish name: %s' % html.escape(item['info']['name']['en'])
            if 'ru' in item['info']['name']:
                message_html_text += '\nRussian name: %s' % html.escape(item['info']['name']['ru'])

        if 'quality' in item['info']:
            message_html_text += '\nQuality: %s' % item['info']['quality']

        if 'year' in item['info']:
            message_html_text += '\nYear: %d' % item['info']['year']

        if 'type' in item['info'] and item['info']['type'] == 'tv-series' and 'episodes' in item['info']:
            message_html_text += '\nTV-series:'
            if 'season' in item['info']['episodes']:
                message_html_text += ' season %d' % item['info']['episodes']['season']
            if 'episode' in item['info']['episodes']:
                message_html_text += ' episode %d' % item['info']['episodes']['episode']
            else:
                if 'episode_from' in item['info']['episodes']:
                    message_html_text += ' episodes %d' % item['info']['episodes']['episode_from']
                if 'episode_to' in item['info']['episodes']:
                    message_html_text += '-%d' % item['info']['episodes']['episode_to']
            if 'total' in item['info']['episodes']:
                message_html_text += ' of %d' % item['info']['episodes']['total']

        if 'imdb' in item:
            if 'imdbRating' in item['imdb']:
                message_html_text += '\nIMDB rating: %s' % item['imdb']['imdbRating']
            message_html_text += '\nIMDB ID: %s' % item['imdb']['imdbID']

        if 'files' in item:
            size = 0
            files = len(item['files'])
            for file_info in item['files']:
                size += file_info['size']
            message_html_text += '\nSize: %s (%d file%s)' % (format_file_size(size),
                                                             files,
                                                             's' if files > 1 else '')

        logging.info('ITEM: %s', item)

        service.call_service_post('service.telegram.bot',
                                  '/chat/%s/message' % user_id, {
                                      'message': message_html_text
                                  })


    def on_event(self, name, data):
        if name == 'event.item.match':
            item = item_db.items.find_one({'_id': data['item_id']})
            if not item:
                logging.error('Item with id %s is not found', data['item_id'])
                return

            match_item = {'user_id': data['user_id'],
                          'link': item['rss']['link'],
                          'title': item['rss']['title']}

            if db.matches.find_one(match_item):
                logging.error('Item with id %s was already posted', data['item_id'])
                return

            match_item['created'] = time.time()
            db.matches.insert_one(match_item)

            self._post_telegram(item, data['user_id'])

#### MAIN ####

if __name__ == '__main__':
    logging.info("Starting service...")
    service.start(listener=TorrentFilterServiceListener())
